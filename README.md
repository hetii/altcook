# ALTCOOK #

### What is this repository for? ###

* Easly combine tools like browserify, watchify, reactify, babelify.
* Generate separate file for vendor libs and project releated libs.
* Generate alt action/store/components files.
* More things will be added soon...

### How do I get set up? ###

* run ./install
* gulp                  -> Just build the code and run httpd
* gulp --action foo     -> Create new alt action skeleton in src/actions/
* gulp --store  foo     -> Create new alt store skeleton in src/stores/
* gulp --components foo -> Create new alt component skeleton in src/components/
* gulp --bulk foo       -> Create all abow in one bulk action
* gulp banner           -> Add header banner information at final index.js file