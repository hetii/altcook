/*
 * This addons are added by me to any project that based on JS to 
 * fix missing and useful methods on standart types.
*/

String.prototype.isNumber = function(){return /^\d+$/.test(this);}
String.prototype.isLetter = function(){return /^[a-zA-Z\(\)]+$/.test(this);}
String.prototype.capitalize = function(notrim) {
    s = notrim ? this : this.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g,'').replace(/\s+/g,' ');
    return s.length > 0 ? s.charAt(0).toUpperCase() + s.slice(1) : s;
}

String.prototype.wordwrap = function (int_width, str_break, cut) {
    //  discuss at: http://phpjs.org/functions/wordwrap/
    // original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // improved by: Nick Callen
    // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: Sakimori
    //  revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // bugfixed by: Michael Grier
    // bugfixed by: Feras ALHAEK
    //   example 1: wordwrap('Kevin van Zonneveld', 6, '|', true);
    //   returns 1: 'Kevin |van |Zonnev|eld'
    //   example 2: wordwrap('The quick brown fox jumped over the lazy dog.', 20, '<br />\n');
    //   returns 2: 'The quick brown fox <br />\njumped over the lazy<br />\n dog.'
    function wordwrap() {
      var str = this;
      var m = ((arguments.length >= 1) ? arguments[0] : 75);
      var b = ((arguments.length >= 2) ? arguments[1] : '\n');
      var c = ((arguments.length >= 3) ? arguments[2] : false);
    
      var i, j, l, s, r;
    
      str += '';
    
      if (m < 1) {
        return str;
      }
    
      for (i = -1, l = (r = str.split(/\r\n|\n|\r/))
        .length; ++i < l; r[i] += s) {
        for (s = r[i], r[i] = ''; s.length > m; r[i] += s.slice(0, j) + ((s = s.slice(j))
          .length ? b : '')) {
          j = c == 2 || (j = s.slice(0, m + 1)
            .match(/\S*(\s)?$/))[1] ? m : j.input.length - j[0].length || c == 1 && m || j.input.length + (j = s.slice(
              m)
            .match(/^\S*/))[0].length;
        }
      }
      return r.join('\n');
    }
    return wordwrap;   
}();

// Inspired by http://bit.ly/juSAWl
// Augment String.prototype to allow for easier formatting.  This implementation 
// doesn't completely destroy any existing String.prototype.format functions,
// and will stringify objects/arrays.
String.prototype.format = function(i, safe, arg) {

  function format() {
    var str = this, len = arguments.length+1;

    // For each {0} {1} {n...} replace with the argument in that position.  If 
    // the argument is an object or an array it will be stringified to JSON.
    for (i=0; i < len; arg = arguments[i++]) {
      safe = typeof arg === 'object' ? JSON.stringify(arg) : arg;
      str = str.replace(RegExp('\\{'+(i-1)+'\\}', 'g'), safe);
    }
    return str;
  }

  // Save a reference of what may already exist under the property native.  
  // Allows for doing something like: if("".format.native) { /* use native */ }
  format.native = String.prototype.format;

  // Replace the prototype property
  return format;

}();

Array.prototype.contains = Array.prototype.contains || function(obj)
{
  var i, l = this.length;
  for (i = 0; i < l; i++)
  {
    if (this[i] == obj) return true;
  }
  return false;
};