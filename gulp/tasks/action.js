var gulp = require('gulp');
var gutil = require('gulp-util');
var config = require('../config').actions;
var action_name = require('./default.js').action;
var pkg = require('./../../package.json');
var header = require('gulp-header');

function string_src(filename, string) {
  var src = require('stream').Readable({ objectMode: true })
  src._read = function () {
    this.push(new gutil.File({ cwd: "", base: "", path: filename, contents: new Buffer(string) }))
    this.push(null)
  }
  return src
}

gulp.task('action', function() {
  
  var template = ["var alt = require('../addons/Alt');","",
    "class <%= name %>Actions {",
    "  constructor() {",
    "    this.generateActions(",
    "      ''",
    "    );",
    "  }",
    "};",
    "module.exports = alt.createActions(<%= name %>Actions);"
  ].join('\n');

  return string_src(action_name+'.jsx', '')
  .pipe(header(template, { name: action_name.capitalize() } ))
  .pipe(gulp.dest(config.dest))

});