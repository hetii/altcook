var pkg = require('./../../package.json');
var config = require('../config').banner;
var header = require('gulp-header');
var gulp = require('gulp');

var banner = ['/**',
' * <%= pkg.name %> - <%= pkg.description %>',
' * @version v<%= pkg.version %>',
' * @link <%= pkg.homepage %>',
' * @license <%= pkg.license %>',
' */',
''].join('\n');

gulp.task('banner', function() {

  return gulp.src(config.src)
  .pipe(header(banner, { pkg: pkg } ))
  .pipe(gulp.dest(config.dest))

});