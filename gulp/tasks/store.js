var gulp = require('gulp');
var gutil = require('gulp-util');
var config = require('../config').stores;
var action_name = require('./default.js').store;
var pkg = require('./../../package.json');
var header = require('gulp-header');

function string_src(filename, string) {
  var src = require('stream').Readable({ objectMode: true })
  src._read = function () {
    this.push(new gutil.File({ cwd: "", base: "", path: filename, contents: new Buffer(string) }))
    this.push(null)
  }
  return src
}

gulp.task('store', function() {
  
  var template = [
    "var alt = require('../addons/Alt');",
    "var <%= name %>Actions = require('../actions/<%= file_name %>');",
    "",
    "class <%= name %>Store {",
    "  constructor() {",
    "    this.bindActions(<%= name %>Actions);",
    "  }",
    "",
    "}",
    "",
    "module.exports = alt.createStore(<%= name %>Store,'<%= name %>Store');"
  ].join('\n');

  return string_src(action_name+'.jsx', '')
  .pipe(header(template, { name: action_name.capitalize(), file_name: action_name } ))
  .pipe(gulp.dest(config.dest))

});