var gulp = require('gulp');
var gutil = require('gulp-util');
var config = require('../config').components;
var action_name = require('./default.js').component;
var pkg = require('./../../package.json');
var header = require('gulp-header');

function string_src(filename, string) {
  var src = require('stream').Readable({ objectMode: true })
  src._read = function () {
    this.push(new gutil.File({ cwd: "", base: "", path: filename, contents: new Buffer(string) }))
    this.push(null)
  }
  return src
}

gulp.task('component', function() {
  
  var template = [
    "var React = require('react');",
    "var ReactStateMagicMixin = require('alt/mixins/ReactStateMagicMixin');",
    "var cxc = require('classnames');",
    "",
    "/* Actions */",
    "var <%= name %>Actions = require('../actions/<%= file_name %>');",
    "",
    "/* Bootstrap */",
    "",
    "/* Components */",
    "",
    "/* Stories */",
    "var <%= name %>Store = require('../stores/<%= file_name %>');",
    "",
    "var NavBarTask = React.createClass({",
    "  mixins: [ReactStateMagicMixin],",
    "  statics: {registerStores:{",
    "    <%= file_name %>: <%= name %>Store,",
    "  }},",
    "",
    "  render() {",
    "",
    "  }",
    "});",
    "",
    "module.exports = NavBarTask;"
  ].join('\n');

  return string_src(action_name+'.jsx', '')
  .pipe(header(template, { name: action_name.capitalize(), file_name: action_name } ))
  .pipe(gulp.dest(config.dest))

});
