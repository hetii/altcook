require('../libs/addons.js');
var gulp = require('gulp');
var tasks = ['build', 'watch', 'server']
var args  = ['action','store', 'component', 'bulk']
var argv  = {}
var cli = []

function getArg(key) {
  var index = process.argv.indexOf(key);
  var next = process.argv[index + 1];
  return (index < 0) ? null : (!next || next[0] === "-") ? true : next;
}

args.forEach(function(t) {
  arg = getArg('--'+t);
  
  if (arg){
    if (!arg.isLetter()){
      console.log("Wrong value of --"+t+" - just letter allowed!");
      tasks = null;
      return;
    }

    if (t == 'action' || t == 'store' || t == 'component'){
      cli.push(t);
      argv[t] = arg;
    } else if (t == 'bulk'){
      cli = ['action','store','component'];
      argv = {
        'action':arg,
        'store':arg,
        'component':arg
      }
    }
  }
});

gulp.task('default', cli.length ? cli : tasks);
module.exports = argv;