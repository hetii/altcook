var config = require('../config').browserify;
var source = require('vinyl-source-stream');
var streamify = require('gulp-streamify');
var browserify = require('browserify');
var connect = require('gulp-connect');
var buffer = require('vinyl-buffer');
var uglify = require('gulp-uglify');
var watchify = require('watchify');
var gutil = require('gulp-util');
var gzip = require('gulp-gzip');
var gulp = require('gulp');

var bundler = watchify(browserify(config.src, watchify.args));

function _bundle(bundler, output_name){

  b = bundler.bundle();
  // log errors if they happen
  b = b.on('error', gutil.log.bind(gutil, 'Browserify Error'));

  if (config.compress) {
    output_name = output_name.substr(0, output_name.lastIndexOf(".")) + ".jgz";
  }
  
  b = b.pipe(source(output_name));

  if (config.uglify){
    b = b.pipe(streamify(uglify()));
  }
  // See: gulp/config.js
  if (config.compress) {
    b = b.pipe(gzip({ gzipOptions: { level: 9}, append:false}));
  }
  b = b.pipe(gulp.dest(config.dest));
  b = b.pipe(connect.reload());
  return b;
}

function bundle() {

  if (config.vendorify) {
    var vendor_packages = require(config.packages).dependencies;
    var vbundler = watchify(browserify(null, watchify.args));

    for (var key in vendor_packages) {
      if (vendor_packages.hasOwnProperty(key)) {
        bundler.external(key);
        vbundler.require(key);
      }
    }
    vbundler.require('./gulp/libs/addons.js');
    _bundle(vbundler, config.vendorName);    
    _bundle(bundler, config.outputName);
  } else {
     bundler.require('./gulp/libs/addons.js');
    _bundle(bundler, config.outputName);
  }
}

config.settings.transform.forEach(function(t) {
    bundler.transform(t);  
});

gulp.task('browserify', bundle);
bundler.on('update', bundle);