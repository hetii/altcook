var dest = './build';
var src = './src';

var url = require('url');
module.exports = {
  server: {
    settings: {
      root: dest,
      host: 'localhost',
      port: 8080,
      livereload: {
        port: 35929
      },
      middleware: function(connect, opt) {
        var d = function(req, res, next) {
            if (req.url.split('.').pop() === "jgz") {
              res.setHeader('Content-Encoding', 'gzip');
            }
            next();
        }
        return [d];
      }
    }
  },
  sass: {
    src: src + '/css/**/*.{sass,scss,css}',
    dest: dest + '/css',
    settings: {
      indentedSyntax: false, // Enable .sass syntax?
      imagePath: '/img' // Used by the image-url helper
    }
  },
  browserify: {

    compress: false,
    uglify: false,
    vendorify: true,
    packages: '../../package.json',

    settings: {
      transform: ['reactify', 'babelify']
    },   

    src: src + '/jsx/index.jsx',
    dest: dest + '/js',
    outputName: 'index.js',
    vendorName: 'vendor.js'
  },
  html: {
    src: src + '/index.html',
    dest: dest
  },
  img: {
    src: src + '/img/**/*.*',
    dest: dest + '/img/'
  },
  watch: {
    src: src + '/**/*.*',
    tasks: ['build']
  },
  banner:{
    src: dest + '/js/index.js',
    dest: dest + '/js/',
  },
  actions:{
    dest: src + '/jsx/actions/'
  },
  stores:{
    dest: src + '/jsx/stores/'
  },
  components:{
    dest: src + '/jsx/components/'
  }
};
